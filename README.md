# simplest Paint in Godot 4

made in [Godot Engine](https://godotengine.org) ``v4.2.1.stable.official [b09f793f5]``

remake of my old [Godot 3 Paint program](https://codeberg.org/sosasees/simplest-paint-in-godot) in Godot 4.<br>
just like the Godot 3 edition,
i did not make this program to be used as-is<br>
but as help for bigger Godot projects — so it's bare-bones.

the old edition runs okay for Godot 3 standards —
Godot 4 standards are higher and i got more skilled,
so this new edition runs _much_ faster.

![screenshot](screenshot.webp)

## features

- pencil
   - draw in black on the white background.<br>
     colors can be changed in the Godot project
- runs _very_ fast
   - simple stupid way to draw lines
      - the line segments are drawn as flat rectangular dots,<br>
        the shape that computers draw the fastest
         - it does not look the best, but good enough for _many_ things
      - nothing needs to be redrawn from scratch,
        it just sticks to an [Image](https://docs.godotengine.org/en/stable/classes/class_image.html)
- hidden Save button
   - when running this project in the Godot editor,
     a Save button appears that saves the image under ``res://saved.webp``

## LICENSES

all scripts in this project are licensed with [MIT License](https://codeberg.org/Sosasees/mit-license/src/branch/2021/LICENSE):
- [``res://main.tscn``](main.tscn)
   - ``Main``
      - ``Canvas:GDScript``
      - ``SaveButton:GDScript``

the icon ([``res://icon.webp``](icon.webp)) by Sosasees is licensed
with [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
